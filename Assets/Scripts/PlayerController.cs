﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {

    Model I_Model;
    SpriteRenderer I_spriteRenderer;

    public GameObject I_bullet;

	private void Awake()
	{
        I_spriteRenderer = GetComponent<SpriteRenderer>();
        I_Model = GetComponent<Model>();
	}


	public override void OnStartLocalPlayer()
	{
    
	}

	void Update () {

        if(!isLocalPlayer){
            return;			
		}

		KeyBoardMovement();

        if(Input.GetKey(KeyCode.Space)){
            Fire();
        }
	}


    void Fire(){
        
        var I_Bullet = Instantiate(I_bullet, this.transform.position, this.transform.rotation);

        I_Bullet.transform.Translate(this.transform.position.x * Time.deltaTime, this.transform.position.y * Time.deltaTime, 0);
        Destroy(I_Bullet, 2.0f);

    }

    void KeyBoardMovement()
    {

        // Inputs

        float xPosition = Input.GetAxis("Horizontal") * Time.deltaTime * I_Model.PlayerMovementspeed;
        float yPosition = Input.GetAxis("Vertical") * Time.deltaTime * I_Model.PlayerMovementspeed;
        transform.Translate(xPosition, yPosition, 0);


#region Flip

        if (xPosition > 0.01f)
        {
            I_spriteRenderer.flipX = false;
        }
        else if (xPosition < -0.01f)
        {
            I_spriteRenderer.flipX = true;
        }

#endregion
    }
}

